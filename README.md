# YT Dislike Count

Recently YouTube [decided to hide the dislike count from videos](https://support.google.com/youtube/thread/134791097/update-to-youtube-dislike-counts).
This datum is till available to the video's owner. This scripts adds a comment to your video with the dislike count,
if you wish to make it public.

## Setup

1. `git clone git@gitlab.com:alejandro/yt-dislike-count.git`
1. `cd yt-dislike-count`
1. `pip install -r requirements.txt`
1. Obtain OAuth2 authorization credentials for the Google API. Follow the steps on https://support.google.com/cloud/answer/6158849
1. Download your OAuth2 client credentials as a JSON file. Place it in the root of this repo and change its name to `client_secret.json`

## Usage

```sh
VIDEO_ID="<your video id>" python main.py
```

The first time you run the command, you'll be instructed to log in to your YouTube account via OAuth.
A comment will be inserted in the video with its current dislike count. Subsequent invocations will update
that comment to reflect the updated dislike count.

You can get the video id from its URL. For example, for https://www.youtube.com/watch?v=d-eCEWxdzfg
you run `VIDEO_ID="d-eCEWxdzfg" python main.py`.

## Run periodically with GitLab CI

To keep your comment automatically up-to-date:

1. Run the command locally at least once. This will create `credentials.json` and `comment_id.txt` in the root of the repository
1. Fork this repository
1. [Add a scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) with the following variables:


| Type     | Key                | Value                            |
|----------|--------------------|----------------------------------|
| File     | CLIENT_SECRET      | [Contents of client_secret.json] |
| File     | CREDENTIALS        | [Contents of credentials.json]   |
| File     | COMMENT_ID         | [Contents of comment_id.txt]     |
| Variable | VIDEO_ID           | [Your video id]                  |

## But like, why?

idk, I had some downtime.
