# -*- coding: utf-8 -*-

import os
import json

import google_auth_oauthlib.flow
import google_auth_oauthlib.helpers
import googleapiclient.discovery
import googleapiclient.errors
import google.oauth2.credentials

scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]

def get_authenticated_client():
    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "client_secret.json"

    try:
        with open("credentials.json","r") as f:
            _, client_config = google_auth_oauthlib.helpers.session_from_client_secrets_file(client_secrets_file, scopes)
            credentials_json = json.load(f)
            credentials = google.oauth2.credentials.Credentials(
                credentials_json["token"],
                refresh_token=credentials_json["refresh_token"],
                token_uri=client_config["installed"]["token_uri"],
                client_id=client_config["installed"]["client_id"],
                client_secret=client_config["installed"]["client_secret"])
    except FileNotFoundError:
        # Get credentials and create an API client
        flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
            client_secrets_file, scopes)
        credentials = flow.run_console()

        with open("credentials.json","x") as f:
            credentials_json = {
                "token": credentials.token,
                "refresh_token": credentials.refresh_token
            }
            json.dump(credentials_json, f)

    return googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

def get_dislike_count(youtube, video_id):
    request = youtube.videos().list(
        id=video_id,
        part="statistics"
    )
    response = request.execute()

    return response["items"][0]["statistics"]["dislikeCount"]

def insert_comment(youtube, video_id, msg):
    request = youtube.commentThreads().insert(
        part="snippet",
        body={
          "snippet": {
            "videoId": video_id,
            "topLevelComment": {
              "snippet": {
                "textOriginal": msg
              }
            }
          }
        }
    )
    response = request.execute()
    with open("comment_id.txt","x") as f:
        f.write(response["id"])


def update_comment(youtube, comment_id, msg):
    request = youtube.comments().update(
        part="snippet",
        body={
          "id": comment_id,
          "snippet": {
            "textOriginal": msg
          }
        }
    )
    response = request.execute()

def main():
    youtube = get_authenticated_client()

    video_id = os.environ.get("VIDEO_ID")
    dislike_count = get_dislike_count(youtube, video_id)
    msg = "Dislike count: %s" % dislike_count
    try:
        with open("comment_id.txt","r") as f:
            update_comment(youtube, f.read(), msg)
    except FileNotFoundError:
        insert_comment(youtube, video_id, msg)

if __name__ == "__main__":
    main()
